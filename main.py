import streamlit as st  # Importing the Streamlit library for building the web app
from datetime import date  # Importing the date class from datetime module

import yfinance as yf  # Importing yfinance for fetching stock data
from prophet import Prophet  # Importing Prophet for time series forecasting
from prophet.plot import plot_plotly  # Importing plot_plotly for plotting Prophet forecast
from plotly import graph_objs as go  # Importing graph objects from Plotly for custom plotting

# Define the start date for fetching historical stock data
START = "2015-01-01"
# Define today's date in the format YYYY-MM-DD
TODAY = date.today().strftime("%Y-%m-%d")

# Set the title of the Streamlit app
st.title("Prediction Application")

# Define a list of stock ticker symbols
stocks = ("AAPL", "GOOG", "MSFT", "GME")
# Create a dropdown menu for selecting the stock ticker
selected_stock = st.selectbox("Select dataset for prediction", stocks)

# Create a slider for selecting the number of years to predict
n_years = st.slider("Years of prediction:", 1, 4)
# Calculate the prediction period in days
period = n_years * 365

# Define a function to load data for the selected stock ticker
@st.cache_data
def load_data(ticker):
    data = yf.download(ticker, START, TODAY)  # Fetch historical stock data
    data.reset_index(inplace=True)  # Reset the index to make 'Date' a column
    return data  # Return the data

# Display a message while loading data
data_load_state = st.text("Load data...")
# Load the data for the selected stock
data = load_data(selected_stock)
# Update the loading message once data is loaded
data_load_state.text("Loading data...done!")

# Display a subheader for raw data section
st.subheader('Raw data')
# Display the last few rows of the data
st.write(data.tail())

# Define a function to plot raw stock data
def plot_raw_data():
    fig = go.Figure()  # Create a new Plotly figure
    # Add a trace for stock opening prices
    fig.add_trace(go.Scatter(x=data['Date'], y=data['Open'], name='stock_open'))
    # Add a trace for stock closing prices
    fig.add_trace(go.Scatter(x=data['Date'], y=data['Close'], name='stock_close'))
    # Update layout to include a title and make the x-axis range slider visible
    fig.layout.update(title_text="Time Series Data", xaxis_rangeslider_visible=True)
    # Display the plot in the Streamlit app
    st.plotly_chart(fig)

# Call the function to plot raw data
plot_raw_data()

# Forecasting
# Prepare the data for training the Prophet model
df_train = data[['Date', 'Close']]
df_train = df_train.rename(columns={"Date": "ds", "Close": "y"})  # Rename columns as required by Prophet

# Initialize the Prophet model
m = Prophet()
# Fit the model on the training data
m.fit(df_train)
# Create a DataFrame to hold future dates for prediction
future = m.make_future_dataframe(periods=period)
# Predict the future values
forcast = m.predict(future)

# Display a subheader for forecast data
st.subheader('Forecast data')
# Display the last few rows of the forecast data
st.write(forcast.tail())

# Display the forecast plot
st.write('forecast data')
fig1 = plot_plotly(m, forcast)  # Create the forecast plot
st.plotly_chart(fig1)  # Display the plot in the Streamlit app

# Display the forecast components
st.write('forecast components')
fig2 = m.plot_components(forcast)  # Create the forecast components plot
st.write(fig2)  # Display the plot in the Streamlit app
