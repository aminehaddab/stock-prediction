# Stock Price Prediction Application

This is a web application built using Streamlit that allows users to select a stock and predict its future prices using the Prophet forecasting model.

## Features

- Select from a predefined list of stocks (AAPL, GOOG, MSFT, GME).
- Choose the number of years to forecast into the future (1 to 4 years).
- Display raw historical stock data.
- Plot time series data for stock prices.
- Forecast future stock prices using the Prophet model.
- Display forecast data and forecast components.

## Installation

To run this application, you need to have Python installed along with the following libraries:

- streamlit
- yfinance
- prophet
- plotly

You can install the required libraries using pip:

```bash
pip install streamlit yfinance prophet plotly
```

## Usage

1. Clone the repository or download the script.
2. Navigate to the directory containing the script.
3. Run the Streamlit app:
```bash
streamlit run main.py
```

4. The app will open in your web browser. Select a stock from the dropdown, choose the prediction period, and view the forecast results.

